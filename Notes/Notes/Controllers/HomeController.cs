﻿using Notes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Notes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Notes()
        {
            IList<Note> n = new List<Note>();
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            var query = from note in db2.notes select note;
            var notes = query.ToList();
            foreach (var no in notes)
            {
                n.Add(new Note() { id = no.Id, date = no.date, title = no.title, note = no.note1});
            }

            ViewBag.Message = "Your contact page.";


            return View(n);
        }
       
        public ActionResult Get(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Note n = db2.notes.Where(x => x.Id == id).Select(x =>
                                                new Note()
                                                {
                                                    id = x.Id,
                                                    date = x.date,
                                                    title = x.title,
                                                    note = x.note1,
                                                    
                                                }).SingleOrDefault();

            return View(n);
        }
        public ActionResult Edit(int id)
        {
            DataClasses1DataContext db2 = new DataClasses1DataContext();
            Note n = db2.notes.Where(x => x.Id == id).Select(x =>
                                                new Note()
                                                {
                                                    id = x.Id,
                                                    date = x.date,
                                                    title = x.title,
                                                    note = x.note1,

                                                }).SingleOrDefault();

            return View(n);
        }
        [HttpPost]
        public ActionResult Add(Note ns)  
        {
            //string connString = ConfigurationManager.ConnectionStrings["contacts"].ToString();

            //DataContext db = new DataContext(connString);
            using (DataClasses1DataContext db2 = new DataClasses1DataContext())
            {
                note n = new note();
                n.date = ns.date; 
                n.title = ns.title;
                n.note1 = ns.note; 
                



                db2.notes.InsertOnSubmit(n);
                db2.SubmitChanges();


            }
            return RedirectToAction("Index");


        }


        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditP(Note no)  
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                note n = db2.notes.Where(x => x.Id == no.id).Single<note>();

                n.date = no.date;
                n.title = no.title;
                n.note1 = no.note; 
                db2.SubmitChanges();
                return RedirectToAction("Notes");
            }
            catch
            {
                return View(no);
            }
        }
        public ActionResult Delete(Note no) 
        {
            try
            {
                DataClasses1DataContext db2 = new DataClasses1DataContext();
                note n = db2.notes.Where(x => x.Id == no.id).Single<note>();
                db2.notes.DeleteOnSubmit(n);
                db2.SubmitChanges();
                return RedirectToAction("Notes");
            }
            catch
            {
                return View(no);
            }

        }

    }
}